# frozen_string_literal: true

FactoryBot.define do
  factory :vulnerability_archived_record, class: 'Vulnerabilities::ArchivedRecord' do
    project
    archive factory: :vulnerability_archive
    sequence(:vulnerability_identifier)
    data do
      {
        report_type: 'sast',
        scanner: 'Find Security Bugs',
        state: 'dismissed',
        severity: 'high',
        title: 'Test Title',
        description: 'Test Description',
        cve_value: 'CVE-2018-1234',
        cwe_value: 'CWE-123',
        created_at: '2025-01-29 19:02:08 UTC',
        location: {
          class: 'com.gitlab.security_products.tests.App',
          end_line: 29,
          file: 'maven/src/main/java/com/gitlab/security_products/tests/App.java',
          method: 'insecureCypher',
          start_line: 29
        },
        resolved_on_default_branch: false,
        notes_summary: 'Test notes summary',
        full_path: 'Test full path',
        cvss: [
          {
            vector: 'CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N',
            vendor: 'GitLab'
          }
        ],
        dismissal_reason: 'false_positive'
      }.deep_stringify_keys
    end
  end
end
